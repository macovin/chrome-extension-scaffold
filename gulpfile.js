var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var watchify = require("watchify");
var tsify = require('tsify');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var paths = {
    pages: {
        config: 
        [
            'src/**/*.json',
            'src/**/*.yml',
        ],
        markup:
        [
            'src/**/*.html',
            'src/**/*.php',
            'src/**/*.xml'
        ],
        assets: {
            sass: 'src/assets/scss/**/*.scss',
            typescript:  'src/assets/ts/**/*.ts',
            images: 'src/assets/img/**/*',
            fonts:  'src/assets/fonts/**/*'
        }            
    }
};




gulp.task("default", [
    "assets", 
    "markup"
]);

gulp.task('assets', [
    "sass", 
    "typescript",
    "images",
    "fonts"
]);

gulp.task('markup', function () {
    return gulp.src(paths.pages.markup.concat(paths.pages.config))
        .pipe(gulp.dest('extension'));
});

gulp.task('sass', function () {
    return gulp.src(paths.pages.assets.sass)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('extension/assets/css/maps'))
        .pipe(gulp.dest('extension/assets/css'));
});

var watchTypescript = function (folder) {
    return watchify(browserify({
                    basedir: './src/assets/ts/',
                    debug: true,
                    entries: [folder],
                    cache: {},
                    packageCache: {}
                })
            .plugin(tsify)
            .transform("babelify"))
            .bundle()
            // .pipe(source(folder+'.js'))
            .pipe(buffer())
            // .pipe(sourcemaps.init({loadMaps: true}))
            // .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('extension/assets/js'));
};

gulp.task('typescript', function () {
    return watchify(browserify({
                basedir: './src/assets/ts/',
                debug: true,
                entries: ['background'],
                cache: {},
                packageCache: {}
            })
            .plugin(tsify)
            .transform("babelify"))
            .bundle()
            .pipe(source('background.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('extension/assets/js')
        );
});



gulp.task('images', function () {
    return gulp.src(paths.pages.assets.images)
        .pipe(gulp.dest('extension/assets/img'));
});
gulp.task('fonts', function () {
    return gulp.src(paths.pages.assets.fonts)
        .pipe(gulp.dest('extension/assets/fonts'));
});





// watchTypescript('app');
// watchedBrowserify.on("update", tsBundle);
// watchedBrowserify.on("log", gutil.log);