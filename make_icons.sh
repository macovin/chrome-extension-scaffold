RAW_DIR=$(pwd)/src/assets/img/raw_icons
DIR=$(pwd)/src/assets/img/icons;
NUMBER_OF_ARGS=${#};
MAX_SIZE="608";
MIN_SIZE="19";

makeIconDir() {
    if [ ! -d ${DIR} ]; then
        echo "in";
        mkdir -p ${DIR};
        return 1;
    fi
    echo "out";
    return 0;
}

convertIcon() {
    SRC=$1
    if [ -d RAW_DIR ]; then
        throwError "The source directory '${RAW_DIR}' does not exist";
    fi
    
    if makeIconDir; then 
        SIZE=$2
        BASE=$(basename ${f});
        FILE_NAME="${BASE%.*}";
        echo "Converting ${f}"
        while [ ${SIZE} -ge ${MIN_SIZE} ]; do
            $(convert ${f} -size ${SIZE}x${SIZE} ${DIR}/${FILE_NAME}_${SIZE}.png)
            echo "${SIZE}x${SIZE} ${BASE}_${SIZE}.png";
            SIZE=$((${SIZE}/2));
        done
    fi

}

convertFolder() {
    for f in ${1}/*; do
        convertIcon ${f} ${MAX_SIZE}
        # convertIcon 
    done
}

throwError() {
    echo $1;
    if [ ${#} -eq 2 ]; then
        exit ${2};
    fi
    exit 1;
}

if [ $(($NUMBER_OF_ARGS % 2)) -eq 0 ]; then 
    if [ ${NUMBER_OF_ARGS} -eq 0 ]; then
        convertFolder ${RAW_DIR};
    else
        COUNTER=1;
        while [ ${COUNTER} -le ${NUMBER_OF_ARGS} ]; do
            CURRENT_ITEM=${1};
            shift;
            NEXT_ITEM=${1};
            convertIcon ${CURRENT_ITEM} ${NEXT_ITEM}
            let COUNTER+=2;
        done
    fi
else
    throwError "You can't have an odd number of arguments!";
fi

